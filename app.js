$( function() {

    // datepicker
    $( ".datepicker" ).datepicker({
        minDate: 1
    });

    // tabs
    $('.tabs .tab').click(function(e) {
        $('.tabs .tab').removeClass('active')
        $(this).addClass('active')
    })

    $( "select:not(.dropdown)" ).selectmenu();

    $('.dropdown').select2();

    $('#nav-icon').click(function() {
        $(this).toggleClass('open')
        $('.nav').toggleClass('active')
        $('.footer-nav').toggleClass('active')
    })

    let calculatorData = [];

    const normalizeArray = (arr) => {
        let result = arr.data.map((item) => {
            const price = item['Price in USD']
            const normalItem = {
                from: item.From,
                to: item.To,
                size: item['Container Size'],
                weight: item.Weight,
                price: Number( price.replace(',', '') )
            }
            return normalItem;
        });
        return result;
    }

    $.get( "./data.json", function( data ) {
        console.log(data)
        calculatorData = normalizeArray(data)
    })

    const calculateHelp = (filters) => {
        let result;
        const filteredItems = calculatorData.filter((item) => {
            if (
                filters.from === item.from &&
                filters.to === item.to &&
                filters.size === item.size
            ) {
                return item
            }
        })

        if (filters.weight) {
            result = filteredItems.filter((item) => {
                const itemWeightArr = item.weight.split('-')
                if (
                    Number(filters.weight) >= Number(itemWeightArr[0]) &&
                    Number(filters.weight) <= Number(itemWeightArr[1])
                ) {
                    return item;
                }
            })
            if (!result.length) {
                $('.weight-error').fadeIn(200)
            }
        } else {
            result = [filteredItems[filteredItems.length - 1]]
        }
        
        return result
    }

    const normlizeString = (str) => {
        const transalte = {
            'ბათუმი': 'BATUMI',
            'თბილისი': 'TBILISI',
            'ბაქო': 'BAKU',
            'ფოთი': 'POTI',
            'ერევანი': 'YEREVAN',
            'გორი': 'GORI',
            'მცხეთა': 'MCKHETA',
            'ნატახტარი': 'NATAKHTARI',
            'ზუგდიდი': 'ZUGDIDI',
            'მარნეული': 'MARNEULI',
            'რუსთავი': 'RUSTAVI',
            'ქუთაისი': 'KUTAISI',
            'ზესტაფონი': 'ZESTAFONI'
        }

        return transalte[str]
    }

    const calculate = () => {
        const from  = normlizeString( $('#from').val() ),
              to    = normlizeString( $('#to').val() ),
              weight= $('#weight').val(),
              date  = $('#date').val(),
              now = new Date(),
              size  = $('#container').val();
        
        if (!date || date > now) {
            $('.date-error').fadeIn(200)
            return false
        } else {
            $('.date-error').fadeOut(200)
        }              

        if (size == "40'HC" || size == "20 REF" || size == "40 REF") {
            $('.size-error').fadeIn(200)
            return false
        } else {
            $('.size-error').fadeOut(200)
        }

        const result = calculateHelp({from, to, weight, size})
        if (result && result[0]) {
            $('.loader').fadeIn(400)
            setTimeout(function() {
                $('.loader').fadeOut(400)
                window.location.href = window.location.href + `/land.html?from=${ $('#from').val() }&to=${ $('#to').val() }&weight=${weight}&size=${size}&price=${result[0].price}`;
            }, 1500)
        }
        
    }

    $('#submit').click(function(e) {
        e.preventDefault();
        calculate()
    })
} );
